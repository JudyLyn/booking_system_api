const jwt = require('jsonwebtoken')
const secret = process.env.SECRET

//create JWT
module.exports.createAccessToken = (user) => {
	//specify the information to be encoded in the JWT payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//sign the JWT using the specified data as the payload
	//"sample of abstraction"
	return jwt.sign(data, secret, {})
}

//verify authenticity of JWT
// next passes on the request to the next middleware function/route/request handler in the stack
module.exports.verify = (req, res, next) => {
	//obtain the JWT from the request header's authorization
	let token = req.headers.authorization

	//console.log(token);

	if(typeof token !== 'undefined'){
		//use JS string method slice() to obtain a string's substring of the authorization property of the header
		//(fixed character(7))
		//used to get the token only
				// removes the "Bearer " from the received token
			
		token = token.slice(7, token.length)
		
		//once we have the token substring, we can now verify it useing the verify() method of the jsonwebtoken package
			//this has to be done SYNCHRONOUSLY for security reasons
		return jwt.verify(token, secret, (err, data)=>{
			//if an error was generated, token is NOT authentic, return with an appropriate error response
			//otherwise proceed to the next statement
			// next() passes the request to the next callback function in the route
			return (err) ? res.send({auth: 'failed'}) : next()
		})
	}else{//there was NO token in the request header
		return res.send({auth: 'failed'})

	}
}

/*=========================================*/

//decode JWT to get information from it (ie. some user info is encoded in the payload)
module.exports.decode = (token) => {//the tokens parameter is for passing in the actual token value as an argument
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) =>{
			//if verification results in error, do NOT decode
			// {complete: true} grabs both the request header and the payload
			return (err) ? null : jwt.decode(token, {complete:true}).payload
		})
	}else{
		return null
	}
}