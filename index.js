const express = require('express')
const cors = require('cors')
const app = express()
//configure the dotenv package to access environment variables via the process.env object
require('dotenv').config()
const mongoose = require('mongoose')
const port = process.env.PORT

//whitelist the port where our NextJS client app will be served from
var corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

//connect to mongodb atlas cloud database
mongoose.connect(process.env.DB_MONGODB, { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})

//set notifications for connection status
let db = mongoose.connection 
//if connection error is encountered, output error in the console
db.on('error', () => {
    console.log('oops something went wrong with our MongoDB connection')
})

//once connected, notify as well
db.once('open', () => {
    console.log('connected to cloud database')
})

//enable processing of JSON request bodies
app.use(express.json())
//enable processing of form data
app.use(express.urlencoded({extended:true}))

//import routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

//assign the imported routes to their respective endpoints
app.use('/api/users', cors(corsOptions), userRoutes)
app.use('/api/courses', cors(corsOptions), courseRoutes)

app.listen(process.env.PORT, () => {
    console.log(`API is now online on port ${process.env.PORT}`)
})
