const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
require('dotenv').config()
const {OAuth2Client} = require('google-auth-library')
const clientId = '471999445841-o1j9p977toka5kelhevugsrri6j06uuf.apps.googleusercontent.com'
const request = require('request')
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)



module.exports.register = (params) => {
    const user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        password: bcrypt.hashSync(params.password, 10),
        loginType: 'email',
        mobileNumber: params.mobileNumber
    })

    
    //the save() method of the mongoose model returns a PROMISE object
        //promises are pending by default and may either resolve or fail
        //until the method/function that gave a promise completes, the promise acts as a placeholder for the possible value that will be returned by the point of origin 
    return user.save()
    //the succeeding statement chained by the then() method of the promise object does NOT need to wait for the completion of the prior statement (see ASYNCHRONOUS JS)
    .then((user, err) => {

        if(user !== null){
            const shortCode = '21584770'
            const accessToken = 'SD5rj6S8FPFj9wyDebjRBxTD3I5JCa2sfRINe75F3r0'
            const clientCorrelator = '123456' //should be a 6 digit number
            const mobileNumber = user.mobileNumber 
            const message = 'You registered successfully!'

            const options = {
                method: 'POST',
                url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
                qs: {'access_token': accessToken},
                headers: {'Content-Type':'application/json'},
                body: {
                    'outboundSMSMessageRequest':{
                        'clientCorrelator': clientCorrelator,
                        'senderAddress': shortCode,
                        'outboundSMSTextMessage': {'message': message},
                        'address': mobileNumber
                    }
                },
                json:true
            }

            //npm install request
            request(options, (error, response, body)=> {
                if(error) throw new Error(error)
                    console.log(body)
            })

            const msg = {
                to: user.email,
                from: 'judybmedalla@gmail.com',
                subject: 'Online-Course-Booking: Congratulations! Crush ka rin niya!',
                text: 'Congrats! Crush ka rin ng Crush mo!',
                html: '<strong>Salamat Shopee!</strong>'
            }
            sgMail.send(msg)
            .then(()=> console.log('send mail success'))
            .catch(()=> console.log('send mail failed'))
        }
        
        return (err) ? false : true
    })
}

module.exports.login = (params) => {
    //find a user with matching email as provided in the request body
    return User.findOne({email: params.email})
    .then(user => {
        if(user === null){//no user found, terminate code
            return false
        }
        //user found, compare the password from request body to that of the user record
        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if(isPasswordMatched){
            //generate a JWT and include it in the response to the client
            return {
                //pass in our user object as an argument to the createAccessToken() function so that the token payload can be created
                accessToken: auth.createAccessToken(user.toObject())//toObject -returns the value of unique keys
            }
        }else{//hashed passwords did NOT match
            return false
        }
    })
}

/*-----------------------------------------------------------------------------*/
//Create a route for getting the details of a user
//step1: find User
//step2: if error return false,
//step3: if the user found, re-assign the password to undefined so it won't be retrieved along with other user data
//step4: return to the user
module.exports.getDetails = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.password = undefined
        return user
    })
}

/*
		method: get
		url: localhost:4000/api/users/details
		authorization: bearer token
		*/

/*==============================================================================*/

//for getStaticPaths
module.exports.getAllIds = () => {
    return User.find()
    .then((users, err) => {
        if (err) return false
        const ids = users.map(user => user._id)
        return ids
    })
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
    //if the verification process is successful, the google account detail shall contain by the data variable, and data variable has a property called 'payload', which contains by the information about the user
    if(data.payload.email_verified === true){
        //Ascynchronous chain, wala syang kinaiba sa User.findOne().then()
        //the .exec() is same as the .then()
        const user = await User.findOne({email: data.payload.email}).exec()
        if(user !== null) {
            //check if the user has a loginType = 'google', return an accesstoken
            if(user.loginType === 'google'){
                return {accessToken: auth.createAccessToken(user.toObject())}
            } else {
                return {error: 'login-type-error'}
            }
        } else {
            const newUser = new User ({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email: data.payload.email,
                loginType: 'google'
            })
            return newUser.save().then((user, err) =>{
                console.log(user)
                return {accessToken: auth.createAccessToken(user.toObject())}
            })
        }
    }else{
        //if the email is failed to verified
        return {error: 'google-auth-error'}
    }
}
/*
-Create a new OAuthClient
-Verify the tokenId given by the payload
-Register a new account using Google response
    -If the user accounte is already registered then we'll return an error message
    -If the user account is already registered and the loginType is google, then we'll create an access token for the user
    -If the user account is not yet registered, then we'll register a new User.
 */
/*
To verify the google token Id, we must first install the google authentication library.

npm install google-auth-library


 */



/*

send grid

curl -i --request POST \
--url https://api.sendgrid.com/v3/mail/send \
--header 'Authorization: Bearer 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' \
--header 'Content-Type: application/json' \
--data '{"personalizations": [{"to": [{"email": "judybmedalla@gmail.com"}]}],"from": {"email": "judybmedalla@gmail.com"},"subject": "Hello, World!","content": [{"type": "text/plain", "value": "Howdy!"}]}'


 */


