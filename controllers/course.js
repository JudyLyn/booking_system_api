const Course = require('../models/course')
const User = require('../models/user')
const request = require('request')

//controller for retrieving all courses
module.exports.getAll = () => {
    //modify to instead return all courses regardless of active status
    return Course.find()
    .then(courses => courses)
}

module.exports.getOne = (params) => {
    return Course.findById(params.id)
    .then((course, err) => {
        if(err) return false
        return course
    })
}

//controller for creating a course (only registered users may do so)
module.exports.add = (params) => {
    if(params.isAdmin){
        const course = new Course({
            name: params.name,
            description: params.description,
            price: params.price
        })
    
        return course.save()
        .then((course, err) => {
            return (err) ? false : true
        })
    }else{
        return false
    }
}

//controller for allowing an admin user to update a course
module.exports.update = (params) => {
    //determine if user is authorized to perform update
    if(params.isAdmin){
        return Course.findById(params.courseId)
        .then((course, err) => {
            console.log(course)
            if(err) return false
            course.name = params.name
            course.description = params.description
            course.price = params.price

            return course.save()
            .then((updatedCourse, err) => {
                console.log(updatedCourse)
                return err ? false : true
            })
        })
    }else{
        return false
    }
}

module.exports.archive = (params) => {
    if(params.isAdmin){
        return Course.findById(params.courseId)
        .then((course, err) => {
            if(err) return false
            //toggle course availability
            if(course.isActive === true){
                course.isActive = false
            }else{
                course.isActive = true
            }
            return course.save()
            .then((updatedCourse, err) => {
                return err ? false : true
            })
        })
    }else{
        return false
    }
}

module.exports.enroll = (params) => {
    console.log(`params: ${params}`)
    if(params.isAdmin){
        return false
    }else{
        return Course.findById(params.courseId)
        .then((course, err) => {
            console.log(`Course: ${course}`)
            if(err) return false
            if(course.isActive){
                const duplicate = course.enrollees.find(enrollee => enrollee.userId === params.userId)
                console.log(`Duplicate: ${duplicate}`)
                if(duplicate){
                    return false
                }else{
                    course.enrollees.push({
                        userId: params.userId
                    })
                    return course.save()
                    .then((updatedCourse, err) => {
                        console.log(`updated course: ${updatedCourse}`)
                        if(err) return false
                        return User.findById(params.userId)
                        .then((user, err) => {
                            if(err) return false
                            user.enrollments.push({
                                courseId: updatedCourse._id
                            })

                       
                            return user.save()
                            .then((updatedUser, err) => {
                                console.log(`updated user: ${updatedUser}`)
                                if(updatedUser !== null){
                                    const shortCode = '21584770'
                                    const accessToken = 'SD5rj6S8FPFj9wyDebjRBxTD3I5JCa2sfRINe75F3r0'
                                    const clientCorrelator = '123456' //should be a 6 digit number
                                    const mobileNumber = updatedUser.mobileNumber 
                                    const message = 'You enrolled successfully!'

                                    const options = {
                                        method: 'POST',
                                        url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
                                        qs: {'access_token': accessToken},
                                        headers: {'Content-Type':'application/json'},
                                        body: {
                                            'outboundSMSMessageRequest':{
                                                'clientCorrelator': clientCorrelator,
                                                'senderAddress': shortCode,
                                                'outboundSMSTextMessage': {'message': message},
                                                'address': mobileNumber
                                            }
                                        },
                                        json:true
                                    }

                                    //npm install request
                                    request(options, (error, response, body)=> {
                                        if(error) throw new Error(error)
                                            console.log(body)
                                    })
                                }
                                return (err) ? false : true


                            })
                        })
                    })
                }
            }else{
                return false
            }
        })
    }
}
