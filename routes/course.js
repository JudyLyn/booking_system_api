const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CourseController = require('../controllers/course')

router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})

router.get('/:id', (req, res) => {
    CourseController.getOne({id: req.params.id}).then(course => res.send(course))
})

//only allow registered users as verified by their JWT access to this endpoint
    //this is done by using the verify function of the auth module as a middleware
router.post('/', auth.verify, (req, res) => {
    const arg = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    CourseController.add(arg).then(result => res.send(result))
})

//route for updating a course
router.put('/:id', auth.verify, (req, res) => {
    //obtain the URL paramater via req.params
    //identify whether the user who sent this request is AUTHORIZED to do so
    const arg = {
        courseId: req.params.id,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    CourseController.update(arg).then(result => res.send(result))
})

router.delete('/:id', auth.verify, (req, res) => {
    const arg = {
        courseId: req.params.id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    CourseController.archive(arg).then(result => res.send(result))
})

router.put('/:courseId/enrollments', auth.verify, (req, res) => {
    const arg = {
        courseId: req.params.courseId,
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    CourseController.enroll(arg).then(result => res.send(result))
})

module.exports = router
