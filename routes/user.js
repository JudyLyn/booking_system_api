const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

/*-------------------------------------------------------------------*/
//get all details note: require auth

//route for getting user info from decoded JWT payload
router.get('/details', auth.verify, (req, res) => {
    //user will have an id, email, and isAdmin properties
    const user = auth.decode(req.headers.authorization)
    //pass in the user id property to a controller function for retrieving all user information
    UserController.getDetails({userId: user.id}).then(result => res.send(result))
})
/*=====================================================================*/

//for getStaticPaths
router.get('/ids', (req, res) => {
    UserController.getAllIds().then(result => res.send(result))
})

//for getStaticProps
router.get('/:id', (req, res) => {
    UserController.getDetails({userId: req.params.id}).then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router
